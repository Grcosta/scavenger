﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    
    public float levelStartDelay = 2f;              //Delay between levels
    public float turnDelay = 0.1f;                  //Delay between turns
    public static GameManager instance = null;      //Singleton for the game manager

    public BoardManager boardScritp;
    private int level = 1;

    public int foodPoints = 100;
    [HideInInspector] public bool playerTurn = true;

    //UI
    private Text levelText;
    private GameObject levelImage;
    private bool doingSetup;                        //While true, player can't move. Set to false so the game can start.

    private List<Enemy> enemies;
    private bool enemiesMoving;

    private bool firstRun = true;

    void Awake()
    {
        //Making a singleton in Unity
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        enemies = new List<Enemy>();
        DontDestroyOnLoad(gameObject);
        boardScritp = GetComponent<BoardManager>();
        InitGame();
    }


    private void OnLevelFinishedLoadding(Scene scene, LoadSceneMode mode)
    {
        if (firstRun)
        {
            firstRun = false;

            return;
        }

        level++;
        InitGame();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoadding;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoadding;
    }

    void InitGame()
    {
        doingSetup = true;

        // UI Setting
        levelImage = GameObject.Find("LevelImage");
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        levelText.text = "Day " + level;
        levelImage.SetActive(true);
        Invoke("HideLevelImage", levelStartDelay);

        //Enemies and level setup;
        enemies.Clear();
        boardScritp.SetupScene(level);
    }

    private void HideLevelImage()
    {
        levelImage.SetActive(false);
        doingSetup = false;
    }

    public void GameOver()
    {
        levelText.text = "After " + level + " days, you starved to death...";
        levelImage.SetActive(true);
        enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerTurn || enemiesMoving || doingSetup)
            return;
        StartCoroutine(MoveEnemies());
    }

    public void AddEnemyToList(Enemy script)
    {
        enemies.Add(script);
    }

    IEnumerator MoveEnemies()
    {
        enemiesMoving = true;
        yield return new WaitForSeconds(turnDelay);
        if(enemies.Count == 0)
        {
            yield return new WaitForSeconds(turnDelay);
        }

        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].MoveEnemy();
            yield return new WaitForSeconds(turnDelay);
        }

        playerTurn = true;
        enemiesMoving = false;
    }


}
